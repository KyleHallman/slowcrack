#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"

//const int PASS_LEN=20;        // Maximum any password will be
//const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hashed = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strcmp(hash, hashed) == 0)
    {
        //printf("Success! \n");
        printf("%s to %s \n", hash , hashed);
        return 1;
        
    }
    else 
    {   
        //printf("Fail \n");
        return 0;
        
    }
        //free(wordhash);
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file. 





char **read_dictionary(char *filename, int *size)
{
    
    
    struct stat info;
    if(stat(filename, &info) == -1)
    {
        printf("Cant stat the file\n");
        exit(1);
    }
    int filesize = info.st_size;
    printf("File is %d bytes\n", filesize);
    
    char *contents = malloc(filesize + 1);
    FILE *in = fopen(filename, "r");
    if(!in)
    {
        printf("Cant open file for reading\n");
        exit(1);
    }
    fread(contents, 1, filesize, in);
    fclose(in);
    contents[filesize] = '\0';
    
    //printf("%s", contents);
    
    int nlcount = 0;
    for(int i = 0; i < filesize; i++)
    {
        if(contents[i] == '\n')
        {
            nlcount++;
        }
    }
        printf("Line count is %d \n", nlcount);
        char ** lines = malloc(nlcount * sizeof(char *));
        
        lines[0] = strtok(contents, "\n");
        int i = 1;
        while((lines[i] = strtok(NULL, "\n")) != NULL)
        {
            i++;
        }
        for (int j = 0; j < nlcount; j++)
        {
            printf("%d %s\n", j, lines[j]);
        }
        *size = nlcount;
        return lines;
    
}


int main(int argc, char *argv[])
{
    
    int size = 20;
    char *hash;
    //char **hashes;
    //char passwords;
    //int nlcount;
    //char *contents;
    int dlen = 100; 
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[2]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    //int dlen;
    char **dict = read_dictionary("rockyou100.txt", &dlen);
    char **hashes = read_dictionary("hashes.txt", &size);
    //printf("starting");
    
    // need to replace this with a nested loop that will compare hashes and citionary 
    // this only compares one hash to the dictionary as a tester
    
     //*hashed = "c33367701511b4f6020ec61ded352059";
    
    //printf("%s\n", dict[0]);
    
    //for (int i = 0; i < dlen; i++)
    //{
      //  if (tryguess(hash, dict[i]) == 1)
    //    {
      //      printf("Got it! %s\n", dict[i]);
        //}
    //}
    //size = 20;
    
    for (int j = 0; j < dlen; j++)
    {
        
        //printf(" d %d  \n", j);
        
        for (int i = 0; i < size; i++) // hashes size
        {
            //printf(" h %d \n", i);
            if(tryguess(hashes[i] , dict[j]) == 1)
            {
                printf("Got it! %s\n", dict[j]);
                break;
                
                
            }
           
            
        }
    }
    //free(contents);
     

    // Open the hash file for reading.


    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
}
